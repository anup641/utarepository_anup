﻿var myApp = angular.module('myApp', []);

myApp.controller('index_navto_login', function ($scope) {



    $scope.login = function () {
        window.location.href = '/ProviderPage';
    };


    $scope.search_selected = function (item) {

        $scope.locationquery=item.name;
        $scope.isReplyFormOpen = false;
        // $searchshow = !$searchshow;
    }
    $scope.showsearch = function () {
        isReplyFormOpen = !isReplyFormOpen;
    }

});
myApp.controller('navigationcontroller', ['$scope', '$http', function ($scope, $http) {
       
        
        $http.get('/api/indexlogin').then(function (response) {
            
            if (response.data.user == "unknown") {
               $('#logindiv').show();
               $('#logout').hide();
            }
            else {
                 $scope.LUN = response.data.user;
               // $('#LUN').append('<i class="fa fa-user"></i><i class="fa f-uppercase f-size-user">' + response.data.user+'</i>');

                $('#logindiv').hide();
                $('#logout').show();
            }


        });
    
    }]);
myApp.controller('indexcontroller', ['$scope', '$http', function ($scope, $http) {
        
        $('#getimg').click(function () {
            
            
            $http.get('/gettestupload').then(function (response) {
                $scope.items = response.data;
            });

        });    
        
        //$('#truesearch').keyup(function () {
        //    var valThis = $(this).val().toLowerCase();
            
        //    console.log(valThis);
        //    var res = $http.get('/api/search', { a: valThis }, function (res) { 
            
        //        console.log(res);
        //    });
            
        //    //res.success(function (data1, status, headers, config) {
                
        //    //    console.log(data);
        //    //});

        ////$('.countryList>li').each(function () {
        ////    var text = $(this).text().toLowerCase();
        ////    (text.indexOf(valThis) == 0) ? $(this).show() : $(this).hide();
        ////});
        //});
        

        
        console.log("controller active");
        $http.get('/api/index').then(function (response) {
            $scope.items = response.data;
            
        });
 
 
    }]);


    /* ----------------------------------- Custom javascript starts -----------------------*/



//$(document).ready(function () {
    

//    //alert("asd");
//    var myelem = null;
//    $("#locsearch").after('<ul id="loculsearch" ></ul>');
    
//    $('#truesearch').keyup(function () {
        
//        $("#truesearch").after('<ul id="ulsearch" ></ul>');

//        var valThis = $(this).val().toLowerCase();
//        $.ajax({
//            type: 'GET',
//            data: valThis,
//            url: 'http://localhost:1337/search?key=' + valThis,
//            dataType: 'JSON',
//            success: function (data) {
//                var ul = $('#ulsearch');
//                // ul.appendTo('#truesearch');id="myli'+i+'"
//                $('#ulsearch li').remove();

//                for (var i = 0; i < data.length; i++) {
//                    if (i > 4) break;
//                    console.log("eachdat:" + data[i].Domain);
//                    myelem = $('<li><a href="#">' + data[i].Domain + '<br />' + '<span class="searchdisc">' + data[i].TrainingName + '</span></a></li>').appendTo(ul);
                    
//                    if (data.length == 0) { $('<li style="padding:10px;">no Result<br />').appendTo(ul); }

//                   // console.log(myelem);
//                }
//            }
//        });
      
//    });
//>>>>>>> master


//    });

//}]);


$(document).ready(function () {
    $("#truesearch").autocomplete( {
        source: function (request, response) {
            $.ajax({
                url: "/search",
                type: "GET",
                data: request,  // request is the value of search input
                success: function (data) {
                    // Map response values to fiedl label and value
                    response($.map(data, function (el) {
                        return {
                            label: el.TrainingName,
                            value: el.TrainingName
                        };
                    }));
                }
            });
        },

        // The minimum number of characters a user must type before a search is performed.
        minLength: 1,

        // set an onFocus event to show the result on input field when result is focused
        focus: function (event, ui) {
            this.value = ui.item.label;
            // Prevent other event from not being execute
            event.preventDefault();
        },
        select: function (event, ui) {
            // Prevent value from being put in the input:
            this.value = ui.item.label;
            // Set the id to the next input hidden field
            $(this).next("input").val(ui.item.value);
            // Prevent other event from not being execute
            event.preventDefault();
            // optionnal: submit the form after field has been filled up
            $('.searchbutton').submit();
        }
    });
});
