// config/auth.js

// expose our config directly to our application using module.exports
module.exports = {
    
    //'facebookAuth' : {
    //	'clientID' 		: '1594659377480307', // your app id
    //	'clientSecret' 	: 'd164a13fab3aae65894ed5b8a90af4e3', // your app secret
    //	'callbackURL' 	: 'http://localhost:1337/auth/facebook/callback'
    //},

    //'linkedinAuth' : {
    //	'clientID' 		: '759zw9dnqbzuyb', // your app id
    //	'clientSecret' 	: 'SBrAR5oia8pAoMuk', // your app secret
    //	'callbackURL' 	: 'http://127.0.0.1:8080/auth/linkedin/callback'
    //},

    //'googleAuth' : {
    //    'clientID' 		: '73116686993.apps.googleusercontent.com',
    //    'clientSecret' 	: 'bQx7xcfwoBWVeD3upMPO8KSc',
    //    'callbackURL' 	: 'http://localhost:1337/auth/google/callback'
    //}

    // 'facebookAuth' : {
    //    'clientID' 		: '1460208970962465', // your app id
    //    'clientSecret' 	: 'beb9f1d0ac448089eb8d1f57612dcdaf', // your app secret
    //    'callbackURL' 	: 'http://127.0.0.1:1337/auth/facebook/callback'
    //}, 

    'facebookAuth' : {
        'clientID' 		: '819110761491116', // your app id
        'clientSecret' 	: '1fcc7a3ebb807682513116add59e2ac3', // your app secret
        'callbackURL' 	: 'http://urtrainingadvisor.com/auth/facebook/callback'
    },

    
    'linkedinAuth' : {
        'clientID' 		: '759zw9dnqbzuyb', // your app id
        'clientSecret' 	: 'SBrAR5oia8pAoMuk', // your app secret
        'callbackURL' 	: 'http://127.0.0.1:8080/auth/linkedin/callback'
    },
    
    'googleAuth' : {
        'clientID' 		: '73116686993.apps.googleusercontent.com',
        'clientSecret' 	: 'bQx7xcfwoBWVeD3upMPO8KSc',
        'callbackURL' 	: 'http://localhost:1337/auth/google/callback'
    }
};