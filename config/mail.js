var Const = require('./constants.js');

module.exports = {

    'sendMail' : function (mailOptions) {

        var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            service: Const.mail.service,
            auth: {
                user: Const.mail.user,
                pass: Const.mail.pass
            }
        });

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Mail sent: ' + info.response);
            }
        });
    }

}