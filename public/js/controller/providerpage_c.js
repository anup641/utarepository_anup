﻿/* The main aim of the page is to do all the crud operation by the provider */

// Making the app modeule
var app = angular.module("ngproviderpage", ['ngFileUpload']);

// Starting the controller action
app.controller("profileupdate", ['$scope', 'myService', 'Upload','$timeout', '$http', function ($scope,myService, Upload, $timeout, $http) {
        
        
        // Getting the basic details fo the provider. The provider details data gets automatically filled on page load
        $http.get('/api/getProviderDetails').then(function (response) {
            $scope.data = response.data;
            myService.set(response.data.userid); // setting userid
            
            // [ GETTING THE COURSES LISTED ON THE NAME OF THE PROVIDER]
            refresh();

        });
        
        var refresh = function () {
            
            var providerid = myService.get();
            $http.get('/api/getCourse/' + providerid).then(function (res) {
                
                console.log("provoderid : " + providerid);
                
                $scope.providerallitems = res.data;
                // console.log(response);
                $('#editdataid').hide();
                $('#courselist').show();
                console.log(res.data);
                console.log(res.data.StatusByA);

            });
        }
        
        
        refresh();
        
        $scope.submitprofile = function () {
            $scope.profile.id = myService.get();
            
            var res = $http.post('/api/ProviderPage/profileupdate', $scope.profile);
            console.log($scope.profile);
            res.success(function (data1, status, headers, config) {
                // $scope.message = data1;
                console.log(data1);
            });
        }
        
        $scope.editcourse = function (courseid) {
            
            $http.get('/api/ProviderPage/editcourse/' + courseid).then(function (res) {
                $('#courselist').hide();
                $('#editdataid').show();
                
                $scope.editdata = res.data[0];


            });
        }
        
        //$scope.editcourse = function (courseid) {
        
        //    $http.get('/api/ProviderPage/editcourse/' + courseid).then(function (res) {
        //        $('#courselist').hide();
        //        $('#editdataid').show();
        
        //        res.success(function (data1, status, headers, config) {
        //            $scope.message = data1;
        //            console.log(data1);
        //            refresh();
        
        //            //  if()
        //            bootbox.alert("<span style='color:green;font-size=large'>Course Added Successfully</span>");
        //        });
        
        
        //       // bootbox.alert("<h4 style='color:rgba(53, 118, 34, 0.83)'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Added successfully</h4>");
        
        //        $scope.editdata = res.data[0];
        
        
        //    });
        //}
        
        $scope.removecourse = function (courseid) {
            
            console.log('remove called');
            $http.post('/ProviderPage/removecourse', { _id: courseid }).success(function (response) {
                $scope.response = response;
                $scope.loading = false;
                bootbox.alert("<span style='color:green;font-size=large'>Course Deleted Successfully</span>");
                refresh();
            });
        /*  var res = $http.post('/ProviderPage/removecourse', {couseid:courseid});

         res.success(function (data1, status, headers, config) {
         $scope.message = data1;
         console.log(data1);
         refresh();
         bootbox.alert("<span style='color:green;font-size=large'>Course Deleted Successfully</span>");
         }); */
        //bootbox.alert("<span style='color:green;font-size=large'>Course Deleted Successfully</span>");
        //refresh();

        }
        
        
        $scope.submitcourse = function (newcourse) {
            $scope.newcourse.userid = myService.get();
            
            // TBF unable to bind data with angular---------------------- SUMIT
            newcourse.Startdate = $('#Startdateadd').val();
            newcourse.Enddate = $('#Enddateadd').val();
            
            console.log(newcourse); 
        var res = $http.post('/ProviderPage/addcourse', newcourse);
            
           // console.log(newcourse);  
            res.error(function (rejection) { 
                alert(rejection);    
            });

        res.success(function (data1, status, headers, config) {
            $scope.message = data1;
            console.log(data1);
            refresh();

            bootbox.alert("<span style='color:green;font-size=large'>Course Added Successfully</span>");
        });


        // bootbox.alert("<h4 style='color:rgba(53, 118, 34, 0.83)'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Added successfully</h4>");


    }
    var timesupdated = 0;
    $scope.updatecourse = function (item) {
        var res = $http.post('/ProviderPage/updatecourse', item);

        res.success(function (data1, status, headers, config) {
            $scope.message = data1;
            timesupdated++;
            console.log(data1);
            if (timesupdated < 2) {
                $('#formupdated').html("Data has been updated successfully");
            }
            else {
                $('#formupdated').html(timesupdated+"Times data has been updated successfully");


            }
            bootbox.alert("<h4 style='color:rgba(53, 118, 34, 0.83)'><i class='fa fa-check fa-3x'></i>&nbsp;&nbsp;&nbsp;Updated successfully</h4>");

            /*  $http.get('/api/getCourse/' + response.data.userid).then(function (res) {
             $scope.providerallitems = res.data;
             console.log(response);
             $('#editdataid').hide();
             console.log(res.data);
             console.log(res.data.StatusByA);

             }); */
            //$('#editdataid').hide();
            // $('#courselist').show();
            refresh();
        });
    };


    $scope.backtolist= function () {
        refresh();
        $('#editdataid').hide();
        $('#courselist').show();
    }


    //$scope.hidable = true;

    // toggle hide and shoe edit items
    $scope.toggle  = function (item) {
        item.show = !item.show;
    }


        $http.get('/api/index').then(function (response) {
            var allitems = response.data;
            var domains = [];
            $.each(allitems, function (index, value) { 
                domains.push(value.training_cat);
            });
               
            $scope.domains = domains;
            
            
        });
        

   // $scope.domains = ['IT-Software', 'Management', 'IT Management', 'Language', 'Telecom'];
   // $scope.domain = $scope.domains[0];

   
        
        
        $scope.getCourses = function (coursename) { 
            $http.get('/api/course_category_items/' + coursename).then(function (response) {
                $scope.courses = response.data[0].cat_datas.split(",");
                console.log(response.data);
            });    
        }
        // $scope.courses = ['java', 'c#', 'nodejs', 'php', 'ruby'];
        //$scope.course = $scope.courses[0];
       
        
        
        
        

    $http.get('/api/indexlogin').then(function (response) {

        console.log(response.data.user);

        if (response.data.user == "unknown") {
            $('#logindiv').show();
            $('#logout').hide();
        }
        else {
            $scope.LUN = response.data.user;
            $('#logindiv').hide();
            $('#logout').show();
        }

        //$scope.hidable = true;

        // toggle hide and shoe edit items
        $scope.toggle  = function (item) {
            item.show = !item.show;
        }


    });

        $http.get('/api/indexlogin').then(function (response) {
            
            console.log(response.data.user);
            
            if (response.data.user == "unknown") {
                $('#logindiv').show();
                $('#logout').hide();
            }
            else {
                $scope.LUN = response.data.user;
                $('#logindiv').hide();
                $('#logout').show();
            }


        });



        /*--------------------- handeling file upload ---------------------------------*/
        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        $scope.log = '';
        console.log($scope.files);
        $scope.upload = function (files) {
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    Upload.upload({
                        url: '/testupload/'+myService.get(),
                        fields: {
                            'username': "asdaSD"
                        },
                        file: file
                    }).progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.log = 'progress: ' + progressPercentage + '% ' +
                                        evt.config.file.name + '\n' + $scope.log;
                    }).success(function (data, status, headers, config) {
                        $timeout(function () {
                            $scope.log = 'file: ' + config.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;

                            refresh();
                        });
                    });
                }
            }
        };
        




    }]);


app.factory('myService', function () {
    var savedData = {}
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }

    return {
        set: set,
        get: get
    }

});

//var app = angular.module('fileUpload', ['ngFileUpload']);

//app.controller('MyCtrl', ['$scope', 'Upload', '$timeout', function ($scope, Upload, $timeout) {
//        $scope.$watch('files', function () {
//            $scope.upload($scope.files);
//        });
//        $scope.log = '';
//        console.log($scope.files);
//        $scope.upload = function (files) {
//            if (files && files.length) {
//                for (var i = 0; i < files.length; i++) {
//                    var file = files[i];
//                    Upload.upload({
//                        url: '/upload',
//                        fields: {
//                            'username': $scope.username
//                        },
//                        file: file
//                    }).progress(function (evt) {
//                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
//                        $scope.log = 'progress: ' + progressPercentage + '% ' +
//                                        evt.config.file.name + '\n' + $scope.log;
//                    }).success(function (data, status, headers, config) {
//                        $timeout(function () {
//                            $scope.log = 'file: ' + config.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;
//                        });
//                    });
//                }
//            }
//        };
//    }]);








