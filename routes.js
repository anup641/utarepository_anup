﻿module.exports = function (app, passport) {
    var url = require('url');
    var MailUtil = require('./config/mail.js');
    var async = require('async');
    var nodemailer = require('nodemailer');
    var crypto = require('crypto');
    var User = require('./app/models/user');
    var mydata = require('./app/models/coursedata');
    
    var coursecategory_api = require('./app/api/course-category_api.js');
    app.use('/api/v1/course-category_api', coursecategory_api);

    var provider_api = require('./app/api/provider_api.js');
    app.use('/api/v1/provider_api', provider_api);

    var institute_api = require('./app/api/institute_api.js');
    app.use('/api/v1/institute_api', institute_api);
    
    var admin_api = require('./app/api/admin_api.js');
    app.use('/api/v1/admin_api', admin_api);
    
    var comman_api = require('./app/api/comman_api.js');
    app.use('/api/v1/comman_api', comman_api);
    
    var upload_api = require('./app/api/upload_api.js');
    app.use('/api/v1/upload_api', upload_api);

    mydata.init(app);
    
    app.get('/help', function (req, res, next) {
        res.send('Nope.. nothing to see here');
    });
    
    //sitemap
    app.get("/sitemap.xml", function (req, res) {
        res.sendfile(__dirname + req.path);
    });
    
    //robots
    app.get("/robots.txt", function (req, res) {
        res.sendfile(__dirname + req.path);
    });
    
    //https hack - Lets see whether it works 
    app.use(function (req, res, next) {
        if (req.isAuthenticated()) {
            res.locals.Greet = "Hi ";
            res.locals.UserName = req.user.seeker_profile.first_name;
            res.locals.Login = "Logout";
        } else {
            res.locals.Greet = "";
            res.locals.UserName = "";
            res.locals.Login = "Login";
        }
       
        //api call log
        //if (req.url.match(/^\/api\/.+/)) {
        //    //var apiLog = require('./app/models/apiLog');
        //    var userid = '';
        //    if (req.user && req.user.userid) userid = req.user.userid;
        //    console.log("Log the call.");
        //    var apiLog = new apiLog({
        //        url : req.url,
        //        ipAddress : req.headers["X-Forwarded-For"] || req.headers["x-forwarded-for"] || req.client.remoteAddress,
        //        userId : userid,
        //    });
        //    apiLog.save(function (err, data) {
        //        if (!err) {
        //            console.log("Log the call.");
        //        }
        //    });
        //}
        next();
    });
    
    
    app.get("/Scripts/*", function (req, res) {
        res.sendfile(__dirname + req.path);
    });
    
    app.get("/js/*", function (req, res) {
        res.sendfile(__dirname + req.path);
    });
    
    app.get("./css/*", function (req, res) {
        res.sendfile(__dirname + req.path);
    });
    
    app.get("/fonts/*", function (req, res) {
        res.sendfile(__dirname + req.path);
    });
    
    app.get("/img/*", function (req, res) {
        res.sendfile(__dirname + req.path);
    });
    
    var Providers = require('./app/models/trainings_category');
    app.get('/api/providers', function (req, res) {
        Providers.find().exec(function (err, data) {
            if (!err) {
                res.write(JSON.stringify(data));
                res.end();
            } else {
                res.status(500).send('Something went wrong while connecting to mongo');
            }
        })
    })
    
    
    // the callback after google has authenticated the user
    
    
    // LOGOUT ==============================
    app.get('/logout', function (req, res) {
        req.session.destroy();        
        res.redirect('/');
    });
    
    // facebook -------------------------------
    // send to facebook to do the authentication    
    //app.get('/auth/facebook_bf', function (req,res) {
    //    req.session.url = req.header('Referer');
    //    res.redirect('/auth/facebook');
    //});
    
    app.get('/auth/facebook', //function (req, res) {
       // console.log(req.url);
        passport.authenticate('facebook', { scope : 'email' }));
    
    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback', 
        passport.authenticate('facebook', {
        successRedirect : '/api/indexlogin',
        failureRedirect : '/'
    }));
    
    // linkedin -------------------------------
    // send to linkedin to do the authentication
    app.get('/auth/linkedin',
        passport.authenticate('linkedin', { state: 'SOME STATE' }),
        function (req, res) {
            // The request will be redirected to Linkedin for authentication, so this
            // function will not be called.
    });

    // google ---------------------------------
    // send to google to do the authentication
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
    
    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
			passport.authenticate('google', {
        successRedirect : '/',
        failureRedirect : '/'
    }));
    
    
    //app.get('/loggedin/:astype', function (req, res) {
    //    if (req.isAuthenticated()) { 
    //        res.redirect('/ProviderPage');
    //    }
    //    else {
    //        if (req.param('astype') == 'asprovider') {
    //            app.locals.usertype = "provider";
    //            res.redirect('/auth/facebook');
    //        }
    //    }
    //    //res.send(req.isAuthenticated() ? req.user : '0');
        
    //}); 
    
    app.get('/redirecDecision',  function (req, res, err) { 
    
        if (req.session.redirect_to) {
            var rel = req.session.redirect_to;
            req.session.redirect_to = null;
            res.redirect(rel);
        } else {
            res.render('index.ejs', {
            });
        }
        //if (req.isAuthenticated()) {
        //    app.locals.Greet = "Hi ";
        //    app.locals.UserName = req.user.seeker_profile.first_name;
        //    app.locals.Login = "Logout";
        //} else {
        //    app.locals.Greet = "";
        //    app.locals.UserName = "";
        //    app.locals.Login = "Login";
        //}
        //if (req.session.redirect_to) {
        //    var rel = req.session.redirect_to;
        //    req.session.redirect_to = null;
        //    res.redirect(rel);
        //} else {
        //    res.render('index.ejs', {
        //    });
        //}
       // req.end();
    });


    var controllers = require("./controllers/index.js");
    controllers.init(app);


}






// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    
    res.redirect('/');
}

//function IsLoggedInToLogin(req, res, next) {
//    //if (req.isAuthenticated())
//        return next();
//    //req.session.redirect_to = req.path;
//    //res.redirect('/login');
//}

// this is deploy test