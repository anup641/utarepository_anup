﻿// server.js
var express = require('express');
var http = require('http');
var app = express();
var port = process.env.PORT || 1337;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var partials = require('express-partials');
var expressValidator = require('express-validator');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var configDB = require('./config/database.js');
//var AWS = require('aws-sdk');

//mongoose connection
mongoose.connect(configDB.url); // connect to our database
require('./config/passport')(passport); // pass passport for configuration
// set up our express application

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/UI_ng'));

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs'); // set up ejs for templating
// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(expressValidator());
app.use(partials());
// Handle 500
app.use(function (error, req, res, next) {
    res.send('500: Internal Server Error', 500);
});
require('./routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
// Create an HTTP service.
http.createServer(app).listen(port);
console.log('UTA Server running at ' + port);